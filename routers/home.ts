import Router from 'koa-router'
import {test} from '../controllers/home'
import {checkParam} from '../middleware/middleware'
import Joi from 'joi';
export const homeRouter = new Router();
let schema = {
    name:Joi.string().required(),
    age:Joi.number().required()
};
homeRouter.post('/haha',checkParam(schema),test);


