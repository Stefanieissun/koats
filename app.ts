import KOA from 'koa'
import ROUTER from 'koa-router'
import bodyParse from 'koa-bodyparser'
import {loadRouter} from './util'
import {errorHandler} from './middleware/middleware'
(async()=>{
    let Router = await loadRouter();
    const app = new KOA();
    app.use(errorHandler);
    const router = new ROUTER({prefix:'/api/'});
    app.use(bodyParse());
    router.use(Router.routes()).use(Router.allowedMethods());
    app.use(router.allowedMethods()).use(router.routes());
    const PORT = process.env.PORT||3000;
    
    app.listen(PORT,()=>console.log(`server is running at ${PORT}`));
})()
