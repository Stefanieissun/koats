/**
 * @author Wang
 * @since 2020-04-04
 * @description web中间件
 */
import {Context, Next} from 'koa'
import Joi from 'joi'
import { join } from 'bluebird';
/**
 * @description 错误处理
 * @param ctx ctx
 * @param next next
 */
export const errorHandler = async (ctx:Context,next:Next)=>{
    try {
        await next();
    } catch (error) {
        //TODO:日志记录和错误分类
        ctx.status = 500;
        ctx.body = {'msg':error.message};
    }
}
/**
 * @description 参数校验
 * @param schema Joi schema
 */
export const checkParam= (schema:Joi.SchemaLike)=>{
    return async (ctx:Context,next:Next)=>{
        let params = JSON.stringify(ctx.query) === '{}'? ctx.request.body:ctx.query;
        let result = Joi.validate(params,schema);
        if(result.error){
            ctx.status = 400;
            ctx.body = {'msg':result.error.message};
            return;
        }
        ctx.state.params = result.value;
        await next();
    }
}