
/**
 * @author Wang
 * @since 2020-04-04
 * @description 路由加载方法
 */
import router from 'koa-router';
import bluebird from 'bluebird'
import { readdir, PathLike } from 'fs';
import {join} from 'path'
const readDir:(path:PathLike)=>Promise<String[]> = bluebird.promisify(readdir);
const Router = new router();


/**
 * @description 动态加载router文件夹下的路由
 */
export const loadRouter = async ()=>{
    let path = join(__dirname,'./routers/');
    let routers = await readDir(path);
    for(let i of routers) {
        if(i!=='index.ts'){
            let path = join(__dirname,`./routers/${i}`);
            const router = await import(path);
            let name = i.split('.')[0];
            Router.use(name,router[`${name}Router`].routes());
        }
    }
    return Router
}
